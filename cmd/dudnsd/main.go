package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/robfig/cron/v3"
)

var domain string
var password string
var host string
var cachedIPFile string

func main() {
	flag.StringVar(&domain, "d", "", "Set the domain to be used by the updater.")
	flag.StringVar(&password, "p", "", "Set the password for the request.")
	flag.StringVar(&host, "h", "", "Set the host for the update request.")
	flag.Parse()

	if domain == "" {
		log.Fatal("domain name required")
	}

	if password == "" {
		log.Fatal("password required")
	}

	if host == "" {
		log.Fatal("host required")
	}

	cachedIPFile = fmt.Sprintf("%s.ip", host)

	checkIP()
	c := cron.New()
	c.AddFunc("@every 15m", func() {
		checkIP()
	})
	c.Start()
	select {}
}

func checkIP() {
	cachedIP, err := getCachedIP()
	if err != nil {
		log.Println("error loading cached ip", err)
		return
	}

	currentIP, err := getCurrentIP()
	if err != nil {
		log.Println("error loading current ip", err)
	}

	if cachedIP == currentIP {
		log.Println("ip address has not changed", currentIP)
		return
	}

	req, err := http.NewRequest("GET", "https://dynamicdns.park-your-domain.com/update", nil)
	if err != nil {
		log.Println("error creating update (get) request", err)
		return
	}

	q := req.URL.Query()
	q.Add("domain", domain)
	q.Add("password", password)
	q.Add("host", host)
	q.Add("ip", currentIP)
	req.URL.RawQuery = q.Encode()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("error making request", err)
		return
	}
	defer resp.Body.Close()

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("error reading update response", err)
		return
	}

	log.Println("Update Response:", string(respBytes))

	if resp.StatusCode == 200 {
		wrtErr := writeIPToCache(currentIP)
		if wrtErr != nil {
			log.Println("error writing current ip to cache", wrtErr)
		}
	}

}

func getCachedIP() (string, error) {
	if _, err := os.Stat(cachedIPFile); os.IsNotExist(err) {
		return "", nil
	}
	cached, opnErr := os.Open(cachedIPFile)
	defer cached.Close()

	if opnErr != nil {
		log.Println("error opening cache ip file %w", opnErr)
		return "", opnErr
	}

	cachedIpBytes, err := ioutil.ReadAll(cached)
	if err != nil {
		log.Println("error reading cache file %w", err)
		return "", err
	}

	return string(cachedIpBytes), nil
}

func writeIPToCache(ip string) error {
	return ioutil.WriteFile(cachedIPFile, []byte(ip), 0644)
}

func getCurrentIP() (string, error) {
	getIpReq, err := http.NewRequest("GET", "https://api.ipify.org", nil)
	if err != nil {
		log.Println("error creating request to get ip %w", err)
		return "", err
	}

	getIpResp, err := http.DefaultClient.Do(getIpReq)
	defer getIpResp.Body.Close()

	currentIpBytes, err := ioutil.ReadAll(getIpResp.Body)

	return string(currentIpBytes), nil
}