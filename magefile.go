// +build mage

package main

import (
	"fmt"
	"os"
	"os/exec"
)

// Default target to run when none is specified
// If not set, running mage will list available targets
// var Default = Build

// A build step that requires additional params, or platform specific steps for example
func Build() error {
	fmt.Println("Building...")
	cmd := exec.Command("go", "build", "-o", "dudns", "cmd/dudnsd/main.go")
	return cmd.Run()
}

// A build step that builds the project for arm
func BuildArm() error {
	fmt.Println("Building arm...")
	cmd := exec.Command("go", "build", "-o", "dudns", "cmd/dudnsd/main.go")
	cmd.Env = append(os.Environ(), "GOOS=linux")
	cmd.Env = append(cmd.Env, "GOARCH=arm")
	return cmd.Run()
}

// Clean up after yourself
func Clean() {
	fmt.Println("Cleaning...")
	os.RemoveAll("dudns")
}
